﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * This component need to be attached to an GameObject with the Animator
 * This class need two Transform objects attached to the feet of the character, with its up axis pointing down on, and forward pointing forward.
 * Each Transform will need to have and AudioSource component attached to it to reproduce the audio clips.
 */

public class BipedalFootsteps : MonoBehaviour {

    [Tooltip("Sprite to instantiate on left footstep")]
    public GameObject leftFootprint;

    [Tooltip("Sprite to instantiate on right footstep")]
    public GameObject rightFootprint;

    [Tooltip("Transform attached to the left foot. Up axis need to face downwards")]
    public Transform leftFootLocation;

    [Tooltip("Transform attached to the right foot. Up axis need to face downwards")]
    public Transform rightFootLocation;

    [Tooltip("Vertical increment of the sprite")]
    public float verticalOffset = 0.05f;

    [Tooltip("Collection of audio clips to reproduce on footstep")]
    public AudioClip[] footStepSounds;

    [Tooltip("Enables random pitch values for the audio source")]
    public bool randomPitch = false;

    [Tooltip("Minimum pitch value for the audio source")]
    public float minimumPitch = 1.0f;

    [Tooltip("Maximum pitch value for the audio source")]
    public float maximumPitch = 1.0f;

    private int lastAudioClipPlayed = -1;

    void PlayRandomFootstepSound(AudioSource audioSource)
    {
        if(audioSource.isPlaying)
        {
            audioSource.Stop();
        }
        if (randomPitch == true)
        {
            audioSource.pitch = Random.Range(minimumPitch, maximumPitch);
        }
        if (footStepSounds.Length == 1)
        {
            audioSource.PlayOneShot(footStepSounds[0]);
        }
        else if (footStepSounds.Length > 1)
        {
            int minIndex = 0;
            int maxIndex = footStepSounds.Length;
            int index = Random.Range(minIndex, maxIndex);
            while (index == lastAudioClipPlayed)
            {
                index = Random.Range(minIndex, maxIndex);
            }
            lastAudioClipPlayed = index;
            audioSource.PlayOneShot(footStepSounds[index]);
        }
    }

    void LeftFootstep()
    {
        RaycastHit hit;

        if (Physics.Raycast(leftFootLocation.position, leftFootLocation.up, out hit))
        {
            Instantiate(leftFootprint, hit.point + hit.normal * verticalOffset, Quaternion.LookRotation(hit.normal, leftFootLocation.forward));
            PlayRandomFootstepSound(leftFootLocation.GetComponent<AudioSource>());
        }
    }

    void RightFootstep()
    {
        RaycastHit hit;

        if (Physics.Raycast(rightFootLocation.position, rightFootLocation.up, out hit))
        {
            Instantiate(rightFootprint, hit.point + hit.normal * verticalOffset, Quaternion.LookRotation(hit.normal, rightFootLocation.forward));
            PlayRandomFootstepSound(rightFootLocation.GetComponent<AudioSource>());
        }
    }
}
