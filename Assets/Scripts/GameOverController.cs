﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverController : MonoBehaviour {
	public bool gameOver = false;
	public GameObject tuto;
	private bool[] playerStarted = new bool[4];
	public GameObject[] playerPrefabs = new GameObject[4];
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();
		
		if(Input.GetKeyDown("joystick 1 button 0") && !playerStarted[0]){
			tuto.SetActive(false);
			Instantiate(playerPrefabs[0]);
            playerStarted[0] = true;
		}
		if(Input.GetKeyDown("joystick 2 button 0") && !playerStarted[1]){
			tuto.SetActive(false);
			Instantiate(playerPrefabs[1]);
			playerStarted[1] = true;
		}
		if(Input.GetKeyDown("joystick 3 button 0") && !playerStarted[2]){
			tuto.SetActive(false);
			Instantiate(playerPrefabs[2]);
			playerStarted[2] = true;
		}
		if(Input.GetKeyDown("joystick 4 button 0") && !playerStarted[3]){
			tuto.SetActive(false);
			Instantiate(playerPrefabs[3]);
			playerStarted[3] = true;
		}
	}

	public void Reload(){
		Invoke("TrueReload", 5);
	}

	private void TrueReload(){
		SceneManager.LoadScene("Intro scene");
	}
	
}
