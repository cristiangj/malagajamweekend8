﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvolveController : MonoBehaviour {
	public GameObject[] involvePrefabs = new GameObject[2];
	public GameOverController gameOverController;
	private MovementController movementController;
	private PlayerAttack playerAttack;
	private PlayerHit playerHit;
	int involveStatusCounter = 0;
	private AudioSource[] involveAudios;
	[Header("Parametros velocipollo")]
	public float velocidadVelocipollo;
	public int rangoAtaqueVelocipollo;
	public float radioAtaqueVelocipollo;
	public float attackLengthVelocipollo;
	public int masaVelocipollo;
	[Header("Parametros tiranopollo")]
	public float velocidadTiranopollo;
	public int rangoAtaqueTiranopollo;
	public float radioAtaqueTiranopollo;
	public float attackLengthTiranopollo;
	public int masaTiranopollo;

	private void Awake() {
		movementController = transform.parent.gameObject.GetComponent<MovementController>();
		playerAttack = transform.parent.gameObject.GetComponent<PlayerAttack>();
		playerHit = transform.parent.gameObject.GetComponent<PlayerHit>();
		involveAudios = GetComponents<AudioSource>();
	}
    public void Involve() {
		involveAudios[involveStatusCounter].Play();
        if (involveStatusCounter <= 1) {
			var childrenPrefab = gameObject.GetComponentsInChildren<Transform>()[1].gameObject;
			var newInvolveStatus = Instantiate (involvePrefabs[involveStatusCounter], transform.position, transform.rotation);
 			newInvolveStatus.transform.parent = gameObject.transform;
			Destroy(childrenPrefab);
			gameObject.GetComponent<PlayerAnimation>().ReloadAnimator();

			if(involveStatusCounter == 0) {
				movementController.speed = velocidadVelocipollo;
				playerAttack.angle = rangoAtaqueVelocipollo;
				playerAttack.radius = radioAtaqueVelocipollo;
				playerAttack.attackLength = attackLengthVelocipollo;
			} else if(involveStatusCounter == 1) {
				movementController.speed = velocidadTiranopollo;
				playerAttack.angle = rangoAtaqueTiranopollo;
				playerAttack.radius = radioAtaqueTiranopollo;
				playerAttack.attackLength = attackLengthTiranopollo;
			}

			playerHit.damageAudioIndex++;
			playerAttack.attackAudioIndex++;
			
		} else {
			var playersArrayLength = GameObject.FindGameObjectsWithTag("PutoPlauer").Length;
			if(playersArrayLength == 2) {
				gameOverController.gameOver = true;
				gameOverController.Reload();
			} else if(playersArrayLength < 2 && !gameOverController.gameOver){
				gameOverController.Reload();
			} else if(playersArrayLength == 1) {
				gameOverController.Reload();
			}
			Destroy(gameObject.transform.parent.gameObject);
		}

		involveStatusCounter++;
    }
}
