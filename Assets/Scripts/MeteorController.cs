﻿using System.Collections;
using UnityEngine;
using DG.Tweening;

public class MeteorController : MonoBehaviour {

	public float timeToFall, timeFalling, timeToDelete;
	public Transform meteorModel;
	public Transform circunference;
    public ParticleSystem crashParticles;
    public ShadowMeteorController shadowMeteorController;
	private AudioSource audioSource;
    public Transform meteorFlames;




    // Use this for initialization
    void Awake () {
        crashParticles.Stop();
		StartCoroutine(ActivateMeteor());
		StartCoroutine(DeleteMeteor());
		audioSource = GetComponent<AudioSource>();
		DOTween.Init();
	}

	private void OnDestroy() {
		StopAllCoroutines();
    }

	private IEnumerator ActivateMeteor() {

		yield return new WaitForSecondsRealtime(timeToFall);

		meteorModel.DOMoveY(-0.5f,timeFalling).OnComplete(() => { MeteorCrash(); }).SetEase(Ease.Linear);
        meteorFlames.DOMoveY(-0.5f, timeFalling);
    }

	private void MeteorCrash() {
		audioSource.Play();
		GetComponentInChildren<SphereCollider>().isTrigger = false;
		shadowMeteorController.projector.gameObject.SetActive(false);
		circunference.gameObject.SetActive(false);

        crashParticles.Play();

		Collider[] colls = Physics.OverlapSphere(circunference.position, transform.localScale.x * 0.5f);

        foreach (Collider col in colls) {
			if (col.gameObject.CompareTag("Player")) {
				col.gameObject.GetComponentInParent<InvolveController>().Involve();
			}
		}
	}

	private IEnumerator DeleteMeteor() {
		yield return new WaitForSecondsRealtime(timeToDelete);
		Destroy(gameObject);
	}
}
