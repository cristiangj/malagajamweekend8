﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MeteorInstantiator : MonoBehaviour {

	public int maxNumInstances = 100;
	public Transform leftCorner,rightCorner;
	public List<string> tagsToAvoid;
	public float minInstantiatingTime, maxInstantiatingTime;
	public float ratioVeloctityInstantiating;
	public AnimationCurve ratioDecrementScale;
	public Vector3 limitScale;
	public GameObject meteorPrefab;

	private Vector3 _point;
	private int _tries = 10000;
	private int _numInstances = 0;
	private int _lastNumInstances = 0;

	//related with ratioDecrementScale
	private Vector3 _nextScale;

	// Use this for initialization
	void Awake () {
		_nextScale = meteorPrefab.transform.localScale;
		StartCoroutine(InstantiateMeteor());
		StartCoroutine(CheckMeteorsInstantiated());
	}

	private IEnumerator CheckMeteorsInstantiated() {

		while (true) {
			yield return new WaitForSecondsRealtime(1f);
			_numInstances = GetComponentsInChildren<Transform>().Length;

			if (_lastNumInstances > _numInstances && _tries == 0) {
				_lastNumInstances = _numInstances;
				_tries = 10000;
				StartCoroutine(InstantiateMeteor());
			}
		}
	}

	private IEnumerator InstantiateMeteor() {

		bool canInstantiate = false;
		GameObject go;

		while (_tries > 0 && !canInstantiate) {

			GetPointInsideRectangle();

			if (IsInsideGameArea()) {
				go = Instantiate(meteorPrefab, _point, Quaternion.identity);
				go.transform.localScale = _nextScale;
				go.transform.Find("Model").rotation = Quaternion.Euler(new Vector3(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f)));
				ReestablishParameters();

				break;
			} else {
				_tries--;
			}
		}

		yield return new WaitForSeconds(Random.Range(minInstantiatingTime,maxInstantiatingTime));

		if (_tries > 0) {
			StartCoroutine(InstantiateMeteor());
		}
	}

	private void GetPointInsideRectangle() {
		_point = new Vector3(Random.Range(leftCorner.position.x, rightCorner.position.x), leftCorner.position.y, Random.Range(leftCorner.position.z, rightCorner.position.z));
	}

	private bool IsInsideGameArea() {
		Collider[] hitColls = Physics.OverlapSphere(_point, _nextScale.x * 0.5f);

		foreach(Collider hit in hitColls) {
			if (tagsToAvoid.Contains(hit.gameObject.tag)) {
				return false;
			}
		}

		return true;
	}

	private void ReestablishParameters() {

		_numInstances++;
		_lastNumInstances = _numInstances;
		_nextScale -= Vector3.one * ratioDecrementScale.Evaluate((float) _numInstances/maxNumInstances);

		if (_nextScale.x < limitScale.x) {
			_nextScale = limitScale;
		}

		minInstantiatingTime -= minInstantiatingTime * ratioVeloctityInstantiating;
		maxInstantiatingTime -= maxInstantiatingTime * ratioVeloctityInstantiating;
		_tries = 10000;
	}
}
