﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour {

	public string horizontal, vertical;
	public float speed = 0.5f;
	public bool stunned = false;

	private float lastHeading = 0;
	private Rigidbody rb;
	private PlayerAnimation _playerAnimation;

	private void Start () {
		rb = GetComponent<Rigidbody> ();
		_playerAnimation = GetComponentInChildren<PlayerAnimation>();
	}
	void Update () {

		float xMovement = Input.GetAxisRaw(horizontal);
		float zMovement = Input.GetAxisRaw(vertical);

		if ((Mathf.Abs(xMovement) > 0.0f || Mathf.Abs(zMovement) > 0.0f) && !stunned) {

			Vector3 movement = new Vector3(xMovement, 0, zMovement);

			_playerAnimation.Run();

			float xRotation = Input.GetAxis(horizontal);
			float zRotation = Input.GetAxis(vertical);

			//float angle = Mathf.Atan2(xMovement, zMovement) * Mathf.Rad2Deg;

			float heading = Mathf.Atan2(xRotation, zRotation);

			//if (heading == 0 && !(movement == Vector3.zero))
				// if (xMovement != 0 && zMovement != 0)
				// 	heading = Mathf.Atan2 (xMovement, zMovement);
				//else
			//	heading = lastHeading;

			lastHeading = heading;

			transform.position = transform.position + movement * speed;
			transform.rotation = Quaternion.Euler (0, heading * Mathf.Rad2Deg, 0);
			//transform.rotation = Quaternion.Euler(0f, angle + Camera.main.transform.eulerAngles.y, 0f);
		} else if (!stunned) {
			_playerAnimation.Idle();
			//rb.freezeRotation = true;
			//rb.freezeRotation = false;
		}

	}
}