﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour {

	private Animator _anim;

	// Use this for initialization
	void Awake () {
		_anim = GetComponentInChildren<Animator>();
	}
	
	public void ReloadAnimator() {
		_anim = GetComponentInChildren<Animator>();
	}

	public void Idle() {
		if(_anim == null) {
			ReloadAnimator();
		} else {
			_anim.SetBool("Run", false);
		}
	}

	public void Run() {
		if(_anim == null) {
			ReloadAnimator();
		} else {
			_anim.SetBool("Run",true);
		}
	}

	public void Attack() {
		if(_anim == null) {
			ReloadAnimator();
		} else {
			_anim.SetTrigger("Attack");
		}
	}

	public void Stunt() {
		if(_anim == null) {
			ReloadAnimator();
		} else {
			_anim.SetTrigger("Stunt");
		}
	}

	public void NotStunt() {
		if(_anim == null) {
			ReloadAnimator();
		} else {
			_anim.SetTrigger("NotStunt");
		}
	}

	public float GetCurrentAnimLength() {
		return _anim.GetCurrentAnimatorStateInfo(0).length;
	}

}
