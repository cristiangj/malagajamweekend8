﻿using System.Collections;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {

	public string attackKey;
	public float attackLength;
	public float cooldown = 0.5f;
	public float angle;

	[Range(0f, 10f)] public float radius;
	public LayerMask hitLayer;
	public bool stunned = false;

	private float _semiAngle { get { return angle * 0.5f; } }
	private float _currentCooldown = 0.0f;
	private PlayerAnimation _playerAnimation;
	private MovementController _playerMovement;
	public int attackAudioIndex = 0;
	private AudioSource[] attackAudios;

	private void Awake() {
		_currentCooldown = 0f;
		_playerAnimation = GetComponentInChildren<PlayerAnimation>();
		_playerMovement = GetComponent<MovementController>();
		attackAudios = GetComponents<AudioSource>();
	}

	private void Update() {
		if (Input.GetButtonDown(attackKey) && _currentCooldown <= 0.0f && !stunned) {

			_playerMovement.stunned = true;
			_playerAnimation.Attack();
			attackAudios[attackAudioIndex].Play();
			StartCoroutine(WaitToMove());
			//CheckHits();
			_currentCooldown = cooldown;
		}

		_currentCooldown -= Time.deltaTime;
	}

	public void Attack() {
		CheckHits();
	}












	private IEnumerator WaitToMove() {
		yield return new WaitForSecondsRealtime(attackLength);
		_playerMovement.stunned = false;
	}

	private void CheckHits() {
		Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius, hitLayer);
		GameObject go;

		foreach (Collider col in hitColliders) {

			go = col.transform.parent.parent.gameObject;

			if (go != gameObject && IsInsideAttackArea(col.transform.parent.parent.position)) {
				col.transform.parent.parent.gameObject.GetComponent<PlayerHit>().Hit(transform.forward);
			}
		}
	}

	private bool IsInsideAttackArea(Vector3 point) {

		if (Vector3.Distance(transform.position, point) <= radius &&
	 		Vector3.Angle(transform.forward, point - transform.position) <= _semiAngle) {

			return true;
		}

		return false;
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.green;
		Gizmos.DrawLine(transform.position, transform.position + transform.forward * radius);
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(transform.position, radius);
	}
}
