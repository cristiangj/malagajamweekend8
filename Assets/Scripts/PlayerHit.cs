﻿using System.Collections;
using UnityEngine;

public class PlayerHit : MonoBehaviour {

	public int displacement;
	public float timeStunned = 1f;
	public GameObject stuntStartsPrefab;

	private Rigidbody _rb;
	private PlayerAnimation _playerAnimation;
	private MovementController _playerMovement;
	private PlayerAttack _playerAttack;
	public int damageAudioIndex = 3;
	private AudioSource[] damageAudios;
	private GameObject stuntStarts;

	// Use this for initialization
	void Awake () {
		stuntStarts = Instantiate(stuntStartsPrefab,transform);
		stuntStarts.SetActive(false);
		_rb = GetComponent<Rigidbody>();
		_playerAnimation = GetComponentInChildren<PlayerAnimation>();
		_playerMovement = GetComponent<MovementController>();
		_playerAttack = GetComponent<PlayerAttack>();
		damageAudios = GetComponents<AudioSource>();
	}
	
	public void Hit(Vector3 direction) {
		_rb.velocity = direction * displacement;
		_playerAnimation.Stunt();
		stuntStarts.SetActive(true);
		damageAudios[damageAudioIndex].Play();
		StartCoroutine(WaitToInput());
	}

	private IEnumerator WaitToInput() {
		_playerMovement.stunned = true;
		_playerAttack.stunned = true;
		yield return new WaitForSecondsRealtime(timeStunned);
		_playerAnimation.NotStunt();
		_playerMovement.stunned = false;
		_playerAttack.stunned = false;
		stuntStarts.SetActive(false);
	}
}
