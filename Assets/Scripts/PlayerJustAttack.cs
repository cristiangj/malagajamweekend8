﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//to be called by animation event
public class PlayerJustAttack : MonoBehaviour {

	private PlayerAttack _playerAttack;
	private bool _playing = false;

	// Use this for initialization
	void Start () {
		_playerAttack = transform.parent.parent.gameObject.GetComponent<PlayerAttack>();
	}

	private void Update() {
		if (!_playerAttack.stunned) {
			_playing = false;
		}
	}

	void Attack() {
		//if (!_playing) {
		//	_playing = true;
			_playerAttack.Attack();
		//}
	}
}
