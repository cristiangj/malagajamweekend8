﻿using UnityEngine;
using DG.Tweening;

public class ShadowMeteorController : MonoBehaviour {

	public float time;
	public Transform limit;
	public Transform projector;

	// Use this for initialization
	void Awake () {
		DOTween.Init();
		projector.transform.DOMoveY(limit.position.y,time);
	}

}
